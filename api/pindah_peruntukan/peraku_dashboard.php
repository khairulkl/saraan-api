<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');


/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

    $DBQueryObj->setSQL_Statement($sqlCurrentPB);

    $DBQueryObj->runSQL_Query();

    if($DBQueryObj->isHavingRecordRow()){
        while($row=$DBQueryObj->fetchRow()){
            /* Manipulating array $row here */
            $index_sesi_pb=$row['index_sesi_pb'];
        }
    }else{
        header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
        echo 'Tiada Prestasi Belanja yang aktif!';
        exit();
    }


/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';


/**TODO: Filter **/
if(!is_null($GET_Data->roleID)  && $GET_Data->roleID!==''){
    $roleID= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->roleID);
}

if($roleID==4){

/*TODO: (8) SQL JUMLAH BARU*/
$sql=<<<SQL
SELECT COUNT(index_permohonan)  AS jumlah_perlu_tindakan
FROM
tbl_permohonan
WHERE status_permohonan = '4' AND index_sesi_pb='$index_sesi_pb'
SQL;

$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['jumlah_perlu_tindakan'] = $row['jumlah_perlu_tindakan'];
} else {
    $summaryFigures['jumlah_perlu_tindakan'] = 0;
}

/*TODO: (8) SQL JUMLAH PERLU TINDAKAN
    $sql=<<<SQL
    SELECT COUNT(index_permohonan) AS tindakan_perakuDua
    FROM
    `tbl_permohonan`
    WHERE status_permohonan ='5'
    SQL;

    //echo $sql;exit;
    $DBQueryObj->setSQL_Statement($sql);
    $DBQueryObj->runSQL_Query();

    if ($DBQueryObj->isHavingRecordRow()) {
        $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
        $summaryFigures['tindakan_perakuDua'] = $row['tindakan_perakuDua'];
    } else {
        $summaryFigures['tindakan_perakuDua'] = 0;
    }*/

/*TODO: (8) SQL SEDANG PROSES*/
$sql=<<<SQL
SELECT COUNT(index_permohonan) AS sedang_proses
FROM
`tbl_permohonan` 
WHERE status_permohonan in ('1', '2', '3', '5') AND index_sesi_pb='$index_sesi_pb'
SQL;

//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['sedang_proses'] = $row['sedang_proses'];
} else {
    $summaryFigures['sedang_proses'] = 0;
}

/*TODO: (8) SQL SELESAI PROSES*/
$sql=<<<SQL
SELECT COUNT(index_permohonan) AS jumlah_selesai_diperaku
FROM
`tbl_permohonan`
WHERE status_permohonan ='6' AND index_sesi_pb='$index_sesi_pb'
SQL;

//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['jumlah_selesai_diperaku'] = $row['jumlah_selesai_diperaku'];
} else {
    $summaryFigures['jumlah_selesai_diperaku'] = 0;
}

}else{

/*TODO: (8) SQL JUMLAH BARU*/
$sql=<<<SQL
SELECT COUNT(index_permohonan)  AS jumlah_perlu_tindakan
FROM
`tbl_permohonan`
WHERE status_permohonan = '5' AND index_sesi_pb='$index_sesi_pb'
SQL;

$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['jumlah_perlu_tindakan'] = $row['jumlah_perlu_tindakan'];
} else {
    $summaryFigures['jumlah_perlu_tindakan'] = 0;
}

/*TODO: (8) SQL SEDANG PROSES*/
$sql=<<<SQL
SELECT COUNT(index_permohonan) AS sedang_proses
FROM
`tbl_permohonan`
WHERE status_permohonan in ('1', '2', '3', '5') AND index_sesi_pb='$index_sesi_pb'
SQL;

//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['sedang_proses'] = $row['sedang_proses'];
} else {
    $summaryFigures['sedang_proses'] = 0;
}

/*TODO: (8) SQL SELESAI PROSES*/
$sql=<<<SQL
SELECT COUNT(index_permohonan) AS jumlah_selesai_diperaku
FROM
`tbl_permohonan`
WHERE status_permohonan ='6' AND index_sesi_pb='$index_sesi_pb'
SQL;

//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();

if ($DBQueryObj->isHavingRecordRow()) {
    $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);
    $summaryFigures['jumlah_selesai_diperaku'] = $row['jumlah_selesai_diperaku'];
} else {
    $summaryFigures['jumlah_selesai_diperaku'] = 0;
}

}

echo json_encode($summaryFigures);