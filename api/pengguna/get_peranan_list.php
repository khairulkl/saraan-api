<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$sql=<<<SQL
SELECT
  `kod_peranan` as value,
  `nama_peranan` as text
FROM `tbl_ref_peranan`
SQL;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
