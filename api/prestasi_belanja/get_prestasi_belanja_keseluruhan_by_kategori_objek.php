<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';

/**TODO: Filter **/
if(!is_null($GET_Data->id_aktiviti)  && $GET_Data->id_aktiviti!==''){
    $id_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->id_aktiviti);
    $condition.="WHERE id_aktiviti = '$id_aktiviti'";    
}

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

$DBQueryObj->setSQL_Statement($sqlCurrentPB);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    while($row=$DBQueryObj->fetchRow()){
        /* Manipulating array $row here */
        $index_sesi_pb=$row['index_sesi_pb'];
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
    echo 'Tiada Prestasi Belanja yang aktif!';
    exit();
}

/*TODO:Include current PB id in query */
if ($condition != '') {
    $condition .= " AND p.index_sesi_pb = '$index_sesi_pb'";
} else {
    $condition .= "WHERE p.index_sesi_pb = '$index_sesi_pb'";
}

$sql=<<<SQL
SELECT
  `kategori_kod_objek`,
  (SELECT
    `perihal_kumpulan_objek`
  FROM
    `tbl_kumpulan_objek_ref`
  WHERE index_kumpulan_objek = p.kategori_kod_objek) AS perihal_kategori_kod_objek,
  SUM(`peruntukan_asal`) AS peruntukan_asal,
  SUM(`peruntukan_dipinda`) AS peruntukan_dipinda,
  SUM(`perbelanjaan_sebenar`) AS perbelanjaan_sebenar,
  SUM(`perbelanjaan_tanggungan`) AS perbelanjaan_tanggungan,
  (SUM(peruntukan_dipinda)-SUM(perbelanjaan_sebenar)-SUM(perbelanjaan_tanggungan)) AS baki,
  (SUM(perbelanjaan_sebenar)/SUM(peruntukan_dipinda)*100) AS peratus_belanja_tanpa_tanggungan,
  ((SUM(perbelanjaan_sebenar)+SUM(perbelanjaan_tanggungan))/SUM(peruntukan_dipinda)*100) AS peratus_belanja_dengan_tanggungan
FROM
  `tbl_prestasi_belanja` p
$condition
 GROUP BY p.`kategori_kod_objek`
SQL;

//echo "$sql";exit();

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
