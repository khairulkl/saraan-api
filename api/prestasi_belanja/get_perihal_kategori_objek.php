<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';

/**TODO: Filter **/
if(!is_null($GET_Data->index_kumpulan_objek)  && $GET_Data->index_kumpulan_objek!==''){
    $index_kumpulan_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->index_kumpulan_objek);
    $condition.="WHERE index_kumpulan_objek = '$index_kumpulan_objek'";    
}

if(!is_null($GET_Data->aktif) && $GET_Data->aktif!==''){
    $aktif= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->aktiif);
    
    if($condition!=''){
        $condition.=" AND aktif = '$aktif'";
    }else{
        $condition.="WHERE aktif = '$aktif'";
    }    
}

$sql=<<<SQL
SELECT
  `perihal_kumpulan_objek`
FROM
  `tbl_kumpulan_objek_ref`
$condition
SQL;

//echo $sql;exit;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
