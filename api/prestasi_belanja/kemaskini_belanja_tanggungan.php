<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../login/EVSessionHandler.php';
include_once '../config/db_connection.php';
include_once '../config/KodTransaksi.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/*TODO: Initializing session*/
$headers = apache_request_headers();

if($headers){
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);


$perbelanjaan_tanggungan=mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->perbelanjaan_tanggungan);
$edit_by=$_SESSION['icno'];
$id_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->id_aktiviti);
$kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_objek);

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

$DBQueryObj->setSQL_Statement($sqlCurrentPB);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    while($row=$DBQueryObj->fetchRow()){
        /* Manipulating array $row here */
        $index_sesi_pb=$row['index_sesi_pb'];
    }
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
    echo 'Tiada Prestasi Belanja yang aktif!';
    exit();
}

$sql = <<<SQL
UPDATE
  `tbl_prestasi_belanja`
SET
  `perbelanjaan_tanggungan` = '{$perbelanjaan_tanggungan}',
  `last_edit_datetime`=NOW(),
  `edit_by`='{$edit_by}'
WHERE `id_aktiviti` = '{$id_aktiviti}'
AND kod_objek = '{$kod_objek}'
AND `index_sesi_pb`='{$index_sesi_pb}'
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {    
    
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    
    if($rowCount!=-1){        
        echo "Successfully updated!";
    }else{
        echo "Operation failed!";
    }
    
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
