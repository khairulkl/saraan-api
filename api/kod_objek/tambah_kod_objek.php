<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$kod_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_objek);
$perihal_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->perihal_objek);
$index_kumpulan_objek= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->index_kumpulan_objek);

//echo $kod_objek;
//exit();

$sqlQuery = <<<SQL
SELECT
  `kod_objek`,
  `perihal_objek`,
  `index_kumpulan_objek`
FROM
  `tbl_objek_ref`
WHERE `kod_objek`='{$kod_objek}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $err='HTTP/1.1 406 Kod Objek telah wujud!';
    header($err);    
    exit();
}

$sql = <<<SQL
INSERT INTO `tbl_objek_ref` (
  `kod_objek`,
  `perihal_objek`,
  `index_kumpulan_objek`
)
VALUES
  (
    '$kod_objek',
    '$perihal_objek',
    '$index_kumpulan_objek'
  );
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    echo "Successfully added!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
