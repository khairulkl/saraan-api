<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$sql=<<<SQL
SELECT
  `index_kumpulan_objek` as value,
  `perihal_kumpulan_objek` as text
FROM
  `tbl_kumpulan_objek_ref`
SQL;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
