<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

//var_dump($GET_Data);

$condition='';

/**TODO: Filter **/
if(!is_null($GET_Data->index_permohonan)  && $GET_Data->index_permohonan!==''){
    $index_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->index_permohonan);
    $condition.="WHERE index_permohonan = '{$index_permohonan}'";
}


$sql = <<<SQL
SELECT 
  `attachmentID`,
  `index_permohonan`,
  `FileName`,
  `FileName_Baru` 
FROM
`tblattachments` 
$condition
SQL;
//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);
$DBQueryObj->runSQL_Query();
$lampiran = [];

while ($row = mysqli_fetch_assoc($DBQueryObj->getQueryResult())) {
    $lampiran[] = $row;
}

echo json_encode($lampiran);