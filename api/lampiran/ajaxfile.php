<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);
// To store uploaded files path
$files_arr = array();

$date = date("Y-m-d");

if(isset($_POST['indexPermohonan'])){
    $index_permohonan=$_POST['indexPermohonan'];
}

if(isset($_POST['userID'])){
    $userID=$_POST['userID'];
}


if(isset($_FILES['files']['name'])){

   // Count total files
   $countfiles = count($_FILES['files']['name']);

   // Upload directory
   $upload_location = "uploads/";

   // Loop all files
   for($index = 0;$index < $countfiles;$index++){

      if(isset($_FILES['files']['name'][$index]) && $_FILES['files']['name'][$index] != ''){
          // File name
          $filename = $_FILES['files']['name'][$index];

          // Get extension
          $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

          // Valid extensions
          $valid_ext = array("png","jpeg","jpg","pdf","txt","doc","docx","PNG");
          $jumFile=$index+1;

          // Check extension
          if(in_array($ext, $valid_ext)){

             // File path
             $newfilename = $date."_".$jumFile."_".$filename;
             $path = $upload_location.$newfilename;

             // Upload file
             if(move_uploaded_file($_FILES['files']['tmp_name'][$index],$path)){
                $files_arr[] = $newfilename;
             }

              $sqlCmdObj = new DBCommand($DBQueryObj);
              $sqlCmdObj->setINSERTintoTable('tblattachments');
              $sqlCmdObj->addINSERTcolumn('index_permohonan', $index_permohonan, IFieldType::INTEGER_TYPE);
              $sqlCmdObj->addINSERTcolumn('userID', $userID, IFieldType::INTEGER_TYPE);
              $sqlCmdObj->addINSERTcolumn('FileName', $filename, IFieldType::STRING_TYPE);
              $sqlCmdObj->addINSERTcolumn('FileName_Baru', $newfilename, IFieldType::STRING_TYPE);
              $sqlCmdObj->addINSERTcolumn('dateUploaded', $date, IFieldType::DATETIME_TYPE);
             //echo $sqlCmdObj->getSQLstring();exit;
              $sqlCmdObj->executeQueryCommand();

              
          }

      }

   }

}

if ($sqlCmdObj->getAffectedRowCount() != -1) {
                $obj = new MagicObject();
                $obj->status = -1;
                $obj->errorMessage = 'OPERASI BERJAYA';

                echo $obj->getJsonString();
              }else{
                $obj = new MagicObject();
                $obj->status = -1;
                $obj->errorMessage = 'OPERASI UNTUK TABLE SEJARAH MASALAH';

                echo $obj->getJsonString();
              }

// $files_arr[]= "BERJAYA";
// echo json_encode($files_arr);
die;
