<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

require_once '../../vendor/autoload.php';
require_once '../config/db_connection.php';
require_once './EVSessionHandler.php';
require_once './EV_Security_Soft.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$credObj = json_decode(file_get_contents("php://input"));
$icno=$credObj->icno;

$DBQueryObj=new DBQuery($host,$username,$password,$database_name);

/*
$DBQueryObjSession=new DBQuery($host,$username,$password,$database_name);
$objSessionHandler = new EVSessionHandler($DBQueryObjSession);
session_set_save_handler($objSessionHandler, true);
$authorization=getallheaders()["Authorization"];
$objSessionHandler->loadSessionData($authorization);
echo "Authorization: " . $authorization . " <pre>";
var_dump($_SESSION);
*/

$loginObj=new EV_Security_Soft($DBQueryObj,IAuthenticationAction::SET_HTTP_RESPONSE_HEADER);

if(isset($credObj->icno) && isset($credObj->password)){
    //echo 'ada ic & password';exit();
    /*Sanitize Input*/
    $credObj->icno=mysqli_real_escape_string($DBQueryObj->getLink(),$credObj->icno);
    $credObj->password=mysqli_real_escape_string($DBQueryObj->getLink(),$credObj->password);

    /* Login to HRMIS*/
    $loginObj->executeLogin($credObj->icno,$credObj->password);   
}else{
     echo 'tak ada ic & password';exit();
    header("{$_SERVER['SERVER_PROTOCOL']} 400 Bad Request");
    $authorizationObj=new MagicObject();
    $authorizationObj->session_id='invalid';
    echo json_encode($authorizationObj);
    exit;
}