<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

require_once '../../vendor/autoload.php';
require_once '../config/db_connection.php';
require_once 'EV_Security.php';
require_once 'EVSessionHandler.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$credObj = json_decode(file_get_contents("php://input"));

$DBQueryObj=new DBQuery($host,$username,$password,$database_name);
$DBQueryObjSession=new DBQuery($host,$username,$password,$database_name);

$objSessionHandler = new EVSessionHandler($DBQueryObjSession);
session_set_save_handler($objSessionHandler, true);

$loginObj=new EV_Security($DBQueryObj,IAuthenticationAction::SET_HTTP_RESPONSE_HEADER);

if(isset($credObj->icno) && isset($credObj->password)){
    //echo 'LOGINNNNNNNN';exit();
    /*Sanitize Input*/
    $credObj->icno=mysqli_real_escape_string($DBQueryObj->getLink(),$credObj->icno);
    $credObj->password=mysqli_real_escape_string($DBQueryObj->getLink(),$credObj->password);

    /* Login to HRMIS*/
    $loginObj->executeLogin($credObj->icno,$credObj->password);    
    echo $loginObj->JSONnizeUserDetails();
   
}else{
    header("{$_SERVER['SERVER_PROTOCOL']} 400 Bad Request");
    exit;
}


/* To check detail from tblusers loaded correctly */
//$loginObj->loadUserDetail($DBQueryObj);

/* Display sessions data*/
//var_dump($_SESSION);

/* Emulate logout
session_regenerate_id(true);
session_unset();
session_destroy();
session_write_close();
setcookie(session_name(), '', 0, '/');*/