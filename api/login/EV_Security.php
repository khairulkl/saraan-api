<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * 
 * User: Mohd Ilhammuddin Bin Mohd Fuead 
 * Date: 26/07/2021
 * Time: 09:20 PM
 */
class EV_Security extends Login {

    public $loggedInUser;
    const BUID_JPA=13280;
    const DB_WORK_DETAILS_IS_LOCALHOST=false;

    /**
     * To set up proper redirection file
     *
     * An method that must be implemented in order to set up proper
     * redirection to support security feature in User class.
     *
     * Utility Method for setting up redirection file (use $this-> to invoke):<br>
     * i.   setAfterLogInPage<br>
     * ii.  setAfterLogoutInPage<br>
     * iii. setNotInSessionPage<br>
     * iv.  setNoAuthorizationForModulPage<br>
     * v.   setInvalidRightRedirectPage<br>
     *
     */
    protected function setRedirectFiles() {
        // TODO: Implement setRedirectFiles() method.
    }

    public function executeLogin($uName, $uPassword) {
        if ($this->isAuthenticated($uName, $uPassword)) {
            $this->setAuthenticate($uName);
            if ($this->httpResponseAction == IAuthenticationAction::REDIRECT) {
                $this->redirect("in");
            } else if ($this->httpResponseAction == IAuthenticationAction::SET_HTTP_RESPONSE_HEADER) {
                header("{$_SERVER['SERVER_PROTOCOL']} 200 OK");
            }
        } else {
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo json_encode($this->getUserDetails());
            exit;
        }
    }

    protected function isAuthenticated($usename, $password) {
        $credObj = new MagicObject();
        $credObj->icno = $usename;
        $credObj->password = $password;

        /* TODO : Check user already register with spmb */

        $sql = <<<SQL
Select userID,kategoripenggunaID,status_aktif 
FROM tbl_pengguna 
WHERE NoKP='$usename'
SQL;

        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();

        if ($this->dbQueryObj->isHavingRecordRow()) {
            $row = $this->dbQueryObj->fetchRow();
            
            if ($row['status_aktif'] == 0) {
                /**TODO: User is inactive in tbl_pengguna */
                $this->addUserDetail('errno', '401');
                $this->addUserDetail('errmsg', 'Status pengguna tidak aktif! Sila berhubung dengan Pentadbir Sistem');
                return false;
            }

            if ($row['kategoripenggunaID'] == 1) {
                /* Login using HRMIS credential */
                return $this->loginHRMIS($credObj);
            } else {
                /* Login using SPMB internal credential */
                return $this->loginSPMB($credObj);
            }
        } else {

            /* TODO : Try login to hrmis and register */
            if ($this->loginHRMIS($credObj, true)) {
                return true;
            } else {
                /* TODO : Clarify user exist in hrmis but wrong password */ 
                if($this->is_HRMIS_user($credObj->icno)){                    
                        // TODO : Logging hrmis user with login unsuccessfull 
                        $this->addUserDetail('errno', '401');
                        $this->addUserDetail('errmsg', 'Login tidak berjaya. Sila semak padanan No. Kad Pengenalan & katalaluan anda.');
                }else{
                    if($this->getUserDetailByKey('errno')!=''){
                        // TODO : Hrmis user login successfull but regristration failed. Error no already assigned in HRMIS new registration attempt
                        $this->addUserDetail('errno', '401');
                        $this->addUserDetail('errmsg', 'Pendaftaran maklumat hrmis tidak berjaya!');
                    }else{
                        // TODO : Logging non hrmis nor spmb valid user login
                        $this->addUserDetail('errno', '401');
                        $this->addUserDetail('errmsg', 'Pengguna tidak wujud. Pastikan No. Kad Pengenalan anda betul. Sila daftar pengguna terlebih dahulu.');
                    } 
                }
                return false;
            }
        }
    }
    
    private function getKodBahagian($namaBahagian) {
        $bhgArr= explode(' ', $namaBahagian);
        $bhg='';
        
        foreach ($bhgArr as $part){
            if($part!='BAHAGIAN'){
                $bhg.="$part ";
            }            
        }
        
        $bhg= trim($bhg);
        
        $sql = <<<SQL
SELECT
  `kod_aktiviti`
FROM
  `tbl_aktiviti_ref`
WHERE perihal_aktiviti LIKE '%$bhg%'
SQL;

        //echo $sql;exit;
        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();
        $kod_aktiviti='';
        if ($this->dbQueryObj->isHavingRecordRow()) {
            $row = $this->dbQueryObj->fetchRow(MYSQLI_ASSOC);
            $kod_aktiviti=$row['kod_aktiviti'];           
        }
        
        return $kod_aktiviti;
    }

    private function getKodProgram($kod_aktiviti) {
//        $bhgArr= explode(' ', $namaBahagian);
//        $bhg='';
//        
//        foreach ($bhgArr as $part){
//            if($part!='BAHAGIAN'){
//                $bhg.="$part ";
//            }            
//        }
//        
//        $bhg= trim($bhg);
        
        $sql = <<<SQL
SELECT
  `kod_program`
FROM
  `tbl_aktiviti_ref`
WHERE kod_aktiviti = '{$kod_aktiviti}'
SQL;

        //echo $sql;exit;
        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();
        $kod_program='';
        if ($this->dbQueryObj->isHavingRecordRow()) {
            $row = $this->dbQueryObj->fetchRow(MYSQLI_ASSOC);
            $kod_program=$row['kod_program'];           
        }
        
        return $kod_program;
    }
    
    public function loadUserDetail($uName) {
        $sql = <<<SQL
SELECT userID,NoKP as icno,buid,roleID,userStatusID,kategoripenggunaID,Nama as name,Emel,NoTelBimbit,NoTelPej,adminStatus,unit,ketuaUnit,bahagian,kod_aktiviti,kod_program,jawatan,session_id
FROM tbl_pengguna
WHERE NoKP='$uName'
SQL;

        $this->dbQueryObj->setSQL_Statement($sql);
        $this->dbQueryObj->runSQL_Query();

        if ($this->dbQueryObj->isHavingRecordRow()) {
            $row = $this->dbQueryObj->fetchRow(MYSQLI_ASSOC);

            foreach ($row as $key => $val) {
                $this->addUserDetail($key, $val);
            }
        }

        $this->sessionizeUserDetails();
      
    }

    private function loadWorkDetail($BUId,$DB_localhost=true) {

        
        if(!$DB_localhost){
            /**TODO: LIVE connection to DB integrasi_hrmis */
            $DBQueryObj=new DBQuery('10.21.1.113','spmb','Spmblive@2022','integrasi_hrmis');
        }else{
            /**TODO: LOCAL connection to DB integrasi_hrmis */
            $DBQueryObj=new DBQuery('localhost','root','','integrasi_hrmis');  
        }
        

$sql = <<<SQL
SELECT
  BUAddr1,
  BUAddr2,
  BUAddr3,
  City,
  State
FROM senarai_agensi_prod
WHERE BUId='$BUId'
SQL;
        
        $DBQueryObj->setSQL_Statement($sql);
        $DBQueryObj->runSQL_Query();

        $alamat=[];

        if ($DBQueryObj->isHavingRecordRow()) {
            $row = $DBQueryObj->fetchRow(MYSQLI_ASSOC);

            foreach ($row as $key => $val) {
                if(strlen($val)>0){
                   $alamat[]=htmlspecialchars(utf8_encode($val)); 
                }                                
            }
        }
        
        $this->addUserDetail('alamat', implode('#',$alamat));
        $this->sessionizeUserDetails();

        unset($DBQueryObj);
        unset($alamat);
    }

    public function loginESILA($credObj) {

        $loggedInUser = json_decode('{"id":"","name":"","icno":"","emel":""}');

        if ($credObj->icno && $credObj->password) {
            $client = new Client();

            try {
                $response = $client->request('POST', 'http://10.21.205.180/sourceFiles/modul/700_restapi/login.php', [
                    'form_params' => [
                        'icno' => $credObj->icno,
                        'password' => $credObj->password
                    ]
                ]);

                $body = (string) $response->getBody();
                $loggedInUser = json_decode($body);

                $this->addUserDetail("id", $loggedInUser->id);
                $this->addUserDetail("name", $loggedInUser->name);
                $this->addUserDetail("icno", $loggedInUser->icno);
                $this->addUserDetail("emel", $loggedInUser->emel);

                $this->loadUserDetail($loggedInUser->icno);

                return true;
            } catch (RequestException $e) {
                if ($e->getResponse()->getStatusCode() == '401') {
                    return false;
                }
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }

    public function loginSPMB($credObj, $doRegister = false) {

        $loggedInUser = json_decode('{"id":"","name":"","icno":"","emel":""}');

        if ($credObj->icno && $credObj->password) {

$sql = <<<SQL
SELECT NoKP,password,confirmation
FROM tbl_pengguna
WHERE NoKP='{$credObj->icno}' AND password='{$credObj->password}'
SQL;

            $this->dbQueryObj->setSQL_Statement($sql);
            $this->dbQueryObj->runSQL_Query();

            if ($this->dbQueryObj->isHavingRecordRow()) {
                
                /**
                 * Userid & Password match
                 * Loading user details
                 */
                
                $this->loadUserDetail($credObj->icno);
                
                return true;
                
                
                /**
                 * To check account status active/not active
                 * Check tbluser.confirmation field for 1
                 * If 0 then account is valid but not activated yet
                 */
                
                $row=mysqli_fetch_assoc($this->dbQueryObj->getQueryResult());
                
                if($row['confirmation']==1){
                    $this->loadUserDetail($credObj->icno);
                    return true;
                }else{
                    $this->addUserDetail('errno', '401');
                    //$this->addUserDetail('errmsg', 'Unauthorized - ' . $loggedInUser->message);
                    $this->addUserDetail('errmsg', 'Sila semak e-mel yang didaftarkan untuk pengaktifan akaun.');
                    return false;
                }
                
            } else {
                $this->addUserDetail('errno', '401');
                //$this->addUserDetail('errmsg', 'Unauthorized - ' . $loggedInUser->message);
                $this->addUserDetail('errmsg', 'Kata Laluan bagi No. Kad Pengenalan yang digunakan tidak sepadan!');
                return false;
            }
        } else {
            return false;
        }
    }

    public function loginHRMIS($credObj, $doRegister = false) {

        $loggedInUser = json_decode('{"id":"","name":"","icno":"","emel":""}');

        if ($credObj->icno && $credObj->password) {
            $client = new Client();

            try {
                /*
                 * hrmis mobile LIVE : https://mobile.eghrmis.gov.my/hrmis_rest/spmb/login
                 * pre-hrmis login for SPMB : http://10.21.0.75/hrmis_rest_spmb/spmb/login
                 * hrmis login baru latest pre 27102022 : 'http://10.21.153.183/hrmis_rest_pre/spmb/login'
                 */
                $response = $client->request('POST', 'http://10.21.153.183/hrmis_rest_pre/spmb/login', [
                    'form_params' => [
                        'icno' => $credObj->icno,
                        'password' => $credObj->password
                    ],
                    'headers' => [
                        'Authorization' => 'getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk'
                    ],
                    'timeout' => 10,
                    'http_errors' => false
                ]);

                $body = (string) $response->getBody();

                /* TODO : Retrieved status code */
                $statusCode = $response->getStatusCode();

                if ($statusCode == 200) {

                    /* TODO : Passed JSON from HRMIS server as loggedInUser obj */
                    $loggedInUser = json_decode($body);

                    if (isset($loggedInUser) && !$loggedInUser->error) {
                        /* TODO : loggedInUser obj doesn't have error prop (Successful response) */      
                        
                        /* TODO : Block Non JPA HRMIS user */
                        if ($loggedInUser->BUID_Agensi!='13280') {
                                /* TODO : Block Non JPA user */
                                $this->addUserDetail('errno', '402');
                                $this->addUserDetail('errmsg', 'Sistem hanya dibuka untuk warga JPA!');
                                return false;
                        }

                        /* TODO : Register user if not yet present in SPMB.tbluser */
                        //echo var_dump($doRegister);exit;
                        //echo var_dump($loggedInUser);exit;
                        if ($doRegister) {
                            if (!$this->registerHrmisUser($loggedInUser)) {
                                /* TODO : Hrmis user login successfull but regristration failed */
                                $this->addUserDetail('errno', '401');
                                $this->addUserDetail('errmsg', 'Penetapan maklumat hrmis tidak berjaya!');
                                return false;
                            }
                            /**TODO: 1st time user registration, status NOT ACTIVE by default */
                            $this->addUserDetail('errno', '401');
                            $this->addUserDetail('errmsg', 'Status pengguna tidak aktif! Sila berhubung dengan Pentadbir Sistem');
                            return false;
                        }else{
                            $updStatus=$this->updateHrmisUser($loggedInUser);
                        }
                        
//                        if ($loggedInUser->BUID_Agensi!='13280') {
//                                /* TODO : Block Non JPA user */
//                                $this->addUserDetail('errno', '402');
//                                $this->addUserDetail('errmsg', 'Sistem hanya dibuka untuk warga JPA!');
//                                return false;
//                        }
                        
                        $this->addUserDetail("id", $loggedInUser->NoKP);
                        $this->addUserDetail("name", $loggedInUser->Nama);
                        $this->addUserDetail("icno", $loggedInUser->NoKP);
                        $this->addUserDetail("telephone", $loggedInUser->NoTelPej);
                        $this->addUserDetail("AgcyBUTitle", $loggedInUser->AgcyBUTitle);
                        $this->addUserDetail("agensi", $loggedInUser->Agensi);
                        $this->addUserDetail("Emel", $loggedInUser->Emel);
                        $this->addUserDetail("NoTelBimbit", $loggedInUser->NoTelBimbit);
                        $this->addUserDetail("BUID_Kementerian", $loggedInUser->BUID_Kementerian);
                        $this->addUserDetail("BUID_Agensi", $loggedInUser->BUID_Agensi);
                        //echo $this->getJSONifiedLastError();exit;
                        $this->loadUserDetail($loggedInUser->NoKP);
                        //echo $this->getJSONifiedLastError();exit;
                        $this->loadWorkDetail($loggedInUser->BUId, self::DB_WORK_DETAILS_IS_LOCALHOST);
                        //echo $this->getJSONifiedLastError();exit;
                    } else {
                        /* TODO : loggedInUser obj have error prop (Successful response but HRMIS side validation failed) */
                        $this->addUserDetail('errno', '401');
                        //$this->addUserDetail('errmsg', 'Unauthorized - ' . $loggedInUser->message);                        

                        if(!isset($loggedInUser)){
                            $this->addUserDetail('errmsg', 'Masalah Teknikal HRMIS (Maklumat CO tidak lengkap)'); 
                        }else{
                            $this->addUserDetail('errmsg', $loggedInUser->message);  
                        }
                        return false;
                    }
                } else {
                    $this->addUserDetail('errno', $statusCode);
                    $reasonPhrase = $response->getReasonPhrase();
                    //$this->addUserDetail('errmsg', 'Unauthorized - ' . $loggedInUser->message);
                    $this->addUserDetail('errmsg', $reasonPhrase);                     
                    
                    return false;
                }


                return true;
            } catch (RequestException $e) {
                //errno 20 = timeout
                $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
                $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
                return false;
            } catch (\Exception $e) {
                $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
                $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
                return false;
            }
        } else {
            $this->addUserDetail('errno', '401');
            $this->addUserDetail('errmsg', 'Bad Request (Incomplete parameter)');
            return false;
        }
    }

    private function is_HRMIS_user($icno) {

        $HrmisUser = json_decode('{"status":"TIDAK"}');

        $client = new Client();

        try {
            // REST API (TRAINING) : http://10.21.0.75/hrmis_rest/spmb/semak/$icno
            // REST API (LIVE) : https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno
            $response = $client->request('GET', "https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno", [
                'headers' => [
                    'Authorization' => 'getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk'
                ],
                'timeout' => 10,
                'http_errors' => false
            ]);

            $body = (string) $response->getBody();

            /* TODO : Retrieved status code */
            $statusCode = $response->getStatusCode();

            if ($statusCode == 200) {

                /* TODO : Passed JSON from HRMIS server as HrmisUser obj */
                $HrmisUser = json_decode($body);

                if ($HrmisUser->status == 'YA') {
                    /* TODO : Is valid HRMIS user but login failed */
                    return true;
                }else{
                    /* TODO : Is invalid HRMIS user */
                    return false;
                }
            } else {
                $this->addUserDetail('errno', $statusCode);
                $reasonPhrase = $response->getReasonPhrase();
                $this->addUserDetail('errmsg', $reasonPhrase);
                return false;
            }

            
        } catch (RequestException $e) {
            //errno 20 = timeout
            $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
            $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
            return false;
        } catch (\Exception $e) {
            $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
            $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
            return false;
        }
    }

    private function is_Non_JPA_Administrator($icno){
        $is_admin=false;

        /**TODO: LIVE connection to DB integrasi_hrmis */
        //$DBQueryObj=new DBQuery('10.21.1.113','spmb',')PL9ok8IJ','integrasi_hrmis');

        /**TODO: LOCAL connection to DB integrasi_hrmis */
        $DBQueryObj=new DBQuery('localhost','root','','integrasi_hrmis');

$sql = <<<SQL
SELECT icno,rolecd FROM senarai_pentadbir_prod
WHERE icno='$icno'
SQL;

        $DBQueryObj->setSQL_Statement($sql);
        $DBQueryObj->runSQL_Query();

        if($DBQueryObj->isHavingRecordRow()){
            $is_admin=true;
        }

        unset($DBQueryObj);

        return $is_admin;

    }

    private function registerHrmisUser($userObj) {
        //echo var_dump($userObj);exit;
        if (isset($userObj->NoKP)) {

            $NoKP = mysqli_real_escape_string($this->dbQueryObj->getLink(),$userObj->NoKP);
            $Nama = mysqli_real_escape_string($this->dbQueryObj->getLink(),$userObj->Nama);
            $NoTelPej = mysqli_real_escape_string($this->dbQueryObj->getLink(),$userObj->NoTelPej);
            $Emel = mysqli_real_escape_string($this->dbQueryObj->getLink(),$userObj->Emel);
            $NoTelBimbit = mysqli_real_escape_string($this->dbQueryObj->getLink(),filter_var($userObj->NoTelBimbit,FILTER_SANITIZE_NUMBER_INT));
            $BUID_Kementerian = $userObj->BUID_Kementerian;
            $BUID_Agensi = $userObj->BUID_Agensi;
            $roleID = 1;
            $userStatusID = 1;
            $kategoripenggunaID = 1;
            $kodBUOrgChart = $userObj->KodBUOrgChart;
            $BUId = $userObj->BUId;
            
            if(property_exists($userObj, 'Jawatan')){
                $jawatan= $userObj->Jawatan;
            }
            
            //var_dump($userObj);exit;
            $agensiArr= explode(',', $userObj->Agensi);
            //var_dump($agensiArr);exit;
            $bahagian='';
            foreach ($agensiArr as &$part){
                $part= strtoupper(trim($part));
                if(strpos($part, 'BAHAGIAN')===0){
                    $bahagian=$part;
                    break;
                }
                else if(strpos($part, 'UNIT')===0){                    
                    if($part=='UNIT KOMUNIKASI KORPORAT'){
                        $bahagian=$part;
                        break;
                    }
                }
                else if(strpos($part, 'PEJABAT')===0){                    
                    if($part=='PEJABAT KETUA PENGARAH PERKHIDMATAN AWAM'){
                        $bahagian=$part;
                        break;
                    }
                }else if(strpos($part, 'INSTITUT')===0){
                    if($part=='INSTITUT TADBIRAN AWAM NEGARA (INTAN)'){
                        $bahagian=$part;
                        break;
                    }
                }
                
            }
            $kod_program='';
            if($bahagian!==''){
              $kod_aktiviti= $this->getKodBahagian($bahagian);
              $kod_program= $this->getKodProgram($kod_aktiviti);  
            }            

            /* TODO : Semak status pengguna sebagai Pentadbir Kementerian/Agensi */
            
            if($BUID_Agensi!=self::BUID_JPA){
                if($this->is_Non_JPA_Administrator($NoKP)){
                    if($BUID_Kementerian==$BUID_Agensi){
                        // Pentadbir Kementerian
                        $roleID=10;
                    }else{
                        // Pentadbir Agensi
                        $roleID=11;
                    }
                }
            }

            /* TODO : Insert maklumat dari table HRMIS ke dalam table tbl_pengguna */

            $sqlCmdObj = new DBCommand($this->dbQueryObj);
            $sqlCmdObj->setINSERTintoTable('tbl_pengguna');
            $sqlCmdObj->addINSERTcolumn('NoKP', $NoKP, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('Nama', $Nama, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('NoTelPej', $NoTelPej, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('Emel', $Emel, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('NoTelBimbit', $NoTelBimbit, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('roleID', $roleID, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('userStatusID', $userStatusID, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('kategoripenggunaID', $kategoripenggunaID, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('KodBUOrgChart', $kodBUOrgChart, IFieldType::STRING_TYPE);
            $sqlCmdObj->addINSERTcolumn('BUId', $BUId, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('BUID_Kementerian', $BUID_Kementerian, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('BUID_Agensi', $BUID_Agensi, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addINSERTcolumn('tarikh_kemaskini', Utilities::getDatetime(), IFieldType::DATETIME_TYPE);
            
            if(property_exists($userObj, 'Jawatan')){
                $sqlCmdObj->addINSERTcolumn('jawatan', $jawatan, IFieldType::STRING_TYPE);
            }
            
            if($bahagian!==''){
               $sqlCmdObj->addINSERTcolumn('bahagian', $bahagian, IFieldType::STRING_TYPE); 
               $sqlCmdObj->addINSERTcolumn('kod_aktiviti', $kod_aktiviti, IFieldType::STRING_TYPE); 
               $sqlCmdObj->addINSERTcolumn('kod_program', $kod_program, IFieldType::STRING_TYPE);
            }            
            
            $sqlCmdObj->executeQueryCommand();

            /* Check if command is successfull */
            if($sqlCmdObj->getExecutionStatus()===true){
                return true;
            }else{
                return false;
            }

        } else {
            return false;
        }
    }

    private function updateHrmisUser($userObj) {
        
        if (isset($userObj->NoKP)) {

            $NoKP = $userObj->NoKP;
            $NoTelPej = $userObj->NoTelPej;
            $Emel = $userObj->Emel;
            $NoTelBimbit = $userObj->NoTelBimbit;
            
            if(property_exists($userObj, 'Jawatan')){
                $jawatan = $userObj->Jawatan;
            }
            
            $BUID_Kementerian = $userObj->BUID_Kementerian;
            $BUID_Agensi = $userObj->BUID_Agensi;
            $roleID = 9;
            $userStatusID = 1;
            $kategoripenggunaID = 1;
            $kodBUOrgChart = $userObj->KodBUOrgChart;
            $BUId = $userObj->BUId;
            //var_dump($userObj);exit;
            $agensiArr= explode(',', $userObj->Agensi);
            //var_dump($agensiArr);exit;
            
            $bahagian='';
            foreach ($agensiArr as &$part){
                $part= strtoupper(trim($part));
                if(strpos($part, 'BAHAGIAN')===0){
                    $bahagian=$part;
                    break;
                }
                else if(strpos($part, 'UNIT')===0){                    
                    if($part=='UNIT KOMUNIKASI KORPORAT'){
                        $bahagian=$part;
                        break;
                    }
                }
                else if(strpos($part, 'PEJABAT')===0){                    
                    if($part=='PEJABAT KETUA PENGARAH PERKHIDMATAN AWAM'){
                        $bahagian=$part;
                        break;
                    }
                }else if(strpos($part, 'INSTITUT')===0){
                    if($part=='INSTITUT TADBIRAN AWAM NEGARA (INTAN)'){
                        $bahagian=$part;
                        break;
                    }
                }
                
            }
            $kod_program='';
            if($bahagian!==''){
              $kod_aktiviti= $this->getKodBahagian($bahagian);
              $kod_program= $this->getKodProgram($kod_aktiviti);  
            }      

            $skipRoleUpdate=false;

            /* TODO : Semak status pengguna sebagai Pentadbir Kementerian/Agensi */
            
            if($BUID_Agensi!=self::BUID_JPA){
                if($this->is_Non_JPA_Administrator($NoKP)){
                    if($BUID_Kementerian==$BUID_Agensi){
                        // Pentadbir Kementerian
                        $roleID=10;
                    }else{
                        // Pentadbir Agensi
                        $roleID=11;
                    }
                }
            }else{
                /* TODO: HRMIS user from JPA, may already have assigned role */
                $skipRoleUpdate=true;
            }

            /* TODO: Insert maklumat dari table HRMIS ke dalam table tbl_pengguna */

            $sqlCmdObj = new DBCommand($this->dbQueryObj);
            $sqlCmdObj->setUPDATEtoTable('tbl_pengguna');
            $sqlCmdObj->addUPDATEcolumn('NoTelPej', $NoTelPej, IFieldType::STRING_TYPE);
            $sqlCmdObj->addUPDATEcolumn('Emel', $Emel, IFieldType::STRING_TYPE);
            $sqlCmdObj->addUPDATEcolumn('NoTelBimbit', $NoTelBimbit, IFieldType::STRING_TYPE);

            /* TODO: update role for non JPA HRMIS user, skipped for JPA HRMIS user */
            if(!$skipRoleUpdate){
                $sqlCmdObj->addUPDATEcolumn('roleID', $roleID, IFieldType::INTEGER_TYPE);
            }            
            
            $sqlCmdObj->addUPDATEcolumn('userStatusID', $userStatusID, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addUPDATEcolumn('kategoripenggunaID', $kategoripenggunaID, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addUPDATEcolumn('KodBUOrgChart', $kodBUOrgChart, IFieldType::STRING_TYPE);
            $sqlCmdObj->addUPDATEcolumn('BUId', $BUId, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addUPDATEcolumn('BUID_Kementerian', $BUID_Kementerian, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addUPDATEcolumn('BUID_Agensi', $BUID_Agensi, IFieldType::INTEGER_TYPE);
            $sqlCmdObj->addUPDATEcolumn('tarikh_kemaskini', Utilities::getDatetime(), IFieldType::DATETIME_TYPE);
            
            if(property_exists($userObj, 'Jawatan')){
                $sqlCmdObj->addUPDATEcolumn('jawatan', $jawatan, IFieldType::STRING_TYPE);
            }            
            
            if($bahagian!=''){
               $sqlCmdObj->addUPDATEcolumn('bahagian', $bahagian, IFieldType::STRING_TYPE); 
               $sqlCmdObj->addUPDATEcolumn('kod_aktiviti', $kod_aktiviti, IFieldType::STRING_TYPE); 
               $sqlCmdObj->addUPDATEcolumn('kod_program', $kod_program, IFieldType::STRING_TYPE);
            }            
            
            $sqlCmdObj->addConditionStatement('NoKP', $NoKP, IFieldType::STRING_TYPE, IConditionOperator::NONE);
            
            $sqlCmdObj->executeQueryCommand();

            //echo $sqlCmdObj->getSQLstring();exit;
            if ($sqlCmdObj->getAffectedRowCount() == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function removeUserDetail($key){
        $this->deleteUserDetail($key);
    }
    
    public function newUserDetail($key, $value){
        $this->addUserDetail($key, $value);
    }
    
    public function getLatestSessionId($icno) {

        $HrmisUser = json_decode('{"status":"TIDAK"}');

        $client = new Client();

        try {
            // REST API (TRAINING) : http://10.21.0.75/hrmis_rest/spmb/semak/$icno
            // REST API (LIVE) : https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno
            $response = $client->request('GET', "https://mobile.eghrmis.gov.my/hrmis_rest/spmb/semak/$icno", [
                'headers' => [
                    'Authorization' => 'getHrmisData eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYWRtaW4iLCJyb2xlcyI6WyJhZG1pbmlzdHJhdG9ycyIsImNhbl9lZGl0X2VudGl0eSIsImNhbl9lZGl0X3dvcmtmbG93IiwiZGV2ZWxvcGVycyJdLCJlbWFpbCI6MTU0ODg5MjgwMCwic3ViIjoiNzQxNmEyOTQtZDMwMy00YTA4LWEzNDktNmIwMmU0ZTJlNjgyIiwibmJmIjoxNDc2OTMyMjY2LCJpYXQiOjE0NjExMjEwNjYsImV4cCI6MTU0ODg5MjgwMCwiYXVkIjoicnhnZW5lcmljIn0.hMq4Sltyy_24DWzVD8rJHJYPgG7Emp5EdCBrESLlNOk'
                ],
                'timeout' => 10,
                'http_errors' => false
            ]);

            $body = (string) $response->getBody();

            /* TODO : Retrieved status code */
            $statusCode = $response->getStatusCode();

            if ($statusCode == 200) {

                /* TODO : Passed JSON from HRMIS server as HrmisUser obj */
                $HrmisUser = json_decode($body);

                if ($HrmisUser->status == 'YA') {
                    /* TODO : Is valid HRMIS user but login failed */
                    return true;
                }else{
                    /* TODO : Is invalid HRMIS user */
                    return false;
                }
            } else {
                $this->addUserDetail('errno', $statusCode);
                $reasonPhrase = $response->getReasonPhrase();
                $this->addUserDetail('errmsg', $reasonPhrase);
                return false;
            }

            
        } catch (RequestException $e) {
            //errno 20 = timeout
            $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
            $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
            return false;
        } catch (\Exception $e) {
            $this->addUserDetail('errno', $e->getHandlerContext()['errno']);
            $this->addUserDetail('errmsg', $e->getHandlerContext()['error']);
            return false;
        }
    }

    private function getJSONifiedLastError(){
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return ' - No errors';
            break;
            case JSON_ERROR_DEPTH:
                return ' - Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                return ' - Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                return ' - Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                return ' - Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                return ' - Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
            default:
                return ' - Unknown JSON error';
            break;
        }
    }


}
