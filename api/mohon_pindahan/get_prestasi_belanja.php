<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}


$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if($_SESSION['roleID']<2){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

$condition='';

/**TODO: Filter **/
if(!is_null($GET_Data->id_aktiviti)  && $GET_Data->id_aktiviti!==''){
    $id_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->id_aktiviti);
    $condition.="WHERE id_aktiviti = '$id_aktiviti'";    
}

if(!is_null($GET_Data->kod_objekDRPD) && $GET_Data->kod_objekDRPD!==''){
    $kod_objekDRPD= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_objekDRPD);
    
    if($condition!=''){
        $condition.=" AND kod_objek = '$kod_objekDRPD'";
    }else{
        $condition.="WHERE kod_objek = '$kod_objekDRPD'";
    }    
}

if(!is_null($GET_Data->kod_objekKPD) && $GET_Data->kod_objekKPD!==''){
    $kod_objekKPD = mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_objekKPD);
    
    if($condition!=''){
        $condition.=" AND kod_objek = '$kod_objekKPD'";
    }else{
        $condition.="WHERE kod_objek = '$kod_objekKPD'";
    }    
}

/*TODO:Get PB current ID */
$index_sesi_pb='';

$sqlCurrentPB=<<<SQL
SELECT
  `index_sesi_pb`  
FROM
  `tbl_tetapan_pb`
WHERE status_sesi=1
SQL;

    $DBQueryObj->setSQL_Statement($sqlCurrentPB);

    $DBQueryObj->runSQL_Query();

    if($DBQueryObj->isHavingRecordRow()){
        while($row=$DBQueryObj->fetchRow()){
            /* Manipulating array $row here */
            $index_sesi_pb=$row['index_sesi_pb'];
        }
    }else{
        header("{$_SERVER['SERVER_PROTOCOL']} 503 Locked");
        echo 'Tiada Prestasi Belanja yang aktif!';
        exit();
    }

    
    if($condition!=''){
        $condition.=" AND index_sesi_pb= '$index_sesi_pb'";
    }else{
        $condition.="WHERE index_sesi_pb = '$index_sesi_pb'";
    }    



$sql=<<<SQL
SELECT
  `index_PB`,
  `id_program`,
  `nama_program`,
  `id_aktiviti`,
  `nama_aktiviti`,
  `kod_objek`,
  `peruntukan_asal`,
  `peruntukan_dipinda`,
  `perbelanjaan_sebenar`,
  `perbelanjaan_tanggungan`,
  `kategori_kod_objek`,
  (peruntukan_dipinda-perbelanjaan_sebenar-perbelanjaan_tanggungan) AS baki
FROM
  `tbl_prestasi_belanja` p
$condition
SQL;
//echo $sql;exit;
$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}
