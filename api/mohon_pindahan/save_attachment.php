<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$saveAttachmentObj = (object) $_POST;

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

// setting limit and path
$maxFilesize = 2 * 1024 * 1024; //5MB
$uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/spmb/api/uploads/' . $yearmonth . '/';

if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/spmb/api/uploads/')) {
    mkdir($_SERVER['DOCUMENT_ROOT'] . '/spmb/api/uploads/', 0777);
}

if (!file_exists($uploadDir)) {
    mkdir($uploadDir, 0777);
}

$uploadFile = $uploadDir . basename($_FILES['file']['name']);
$uploadFileType = strtolower(pathinfo(basename($_FILES['file']['name']), PATHINFO_EXTENSION));

$allowedFileTypes = array(
    'pdf',
    'doc',
    'docx',
    'xls',
    'xlsx',
    'ppt',
    'pptx',
    'jpg',
    'jpeg',
    'png',
    'csv',
);

if (in_array($uploadFileType, $allowedFileTypes)) {
    if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile) && ($_FILES['file']['size'] <= $maxFilesize)) {
        $sqlQueryObj->setSELECTQuery('tblrefattachementtypes');
        $sqlQueryObj->addReturnField('attachementTypeID');
        $sqlQueryObj->addConditionField('attachmentTypeExt', '.' . $uploadFileType, IFieldType::STRING_TYPE, IConditionOperator::NONE);
        $DBQueryObj->setSQL_Statement($sqlQueryObj->getSQLQuery());
        $DBQueryObj->runSQL_Query();

        if ($DBQueryObj->isHavingRecordRow()) {
            $row = mysqli_fetch_array($DBQueryObj->getQueryResult());
            $attachmentTypeID = $row[0];
        } else {
            $attachmentTypeID = 'NULL';
        }

        $filename = str_replace(",", "-", basename($uploadFile));


    } else {

    }
}
