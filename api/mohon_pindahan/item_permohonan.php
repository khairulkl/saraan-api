<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';
include_once './item_permohonan_paging.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}


const PAGING_SIZE=10;
$condition='';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();
//$GET_Data->copy_RAW_JSON_properties();
//echo $itemPermohonanOBJ = $GET_Data->getJsonString();

/**TODO: Filter **/
if(!is_null($GET_Data->index_permohonan)  && $GET_Data->index_permohonan!==''){
    $index_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->index_permohonan);
    $condition.="WHERE index_permohonan = '$index_permohonan'";  
}
  


//echo "condition".$condition;exit;

$sql=<<<SQL
SELECT
`id_aktivity`,
  (SELECT perihal_aktiviti FROM tbl_aktiviti_ref WHERE kod_aktiviti=p.id_aktivity) AS `nama_aktiviti`,
`id_pegawai_memohon`,
DATE_FORMAT(tarikh_permohonan, "%d / %m / %Y") as tarikh_permohonan,
DATE_FORMAT(tarikh_proses, "%d / %m / %Y") as tarikh_proses,
DATE_FORMAT(tarikh_dokumen, "%d / %m / %Y") as tarikh_dokumen,
no_rujukan_permohonan,
 (SELECT Nama FROM tbl_pengguna WHERE NoKP=p.id_pegawai_memohon) AS nama_pegawai_pemohon,
justifikasi_permohonan,
`justifikasi_pindaan` as justifikasi_penyedia ,
(SELECT Nama FROM tbl_pengguna WHERE NoKP=p.id_pegawai_syor) AS nama_pegawai_syor,
Perihal_waran
FROM
  tbl_permohonan p
$condition
SQL;
//echo $sql;exit;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '[]';
}