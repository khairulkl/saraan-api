<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';
include_once './KodAktivitiPaging.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->currentPage)){
    $setCurrentPage=$GET_Data->currentPage;
}

/**TODO: Filter **/
if(!is_null($GET_Data->kod_aktiviti)  && $GET_Data->kod_aktiviti!==''){
    $kod_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_aktiviti);
    $condition.="WHERE kod_aktiviti LIKE '%$kod_aktiviti%'";    
}

if(!is_null($GET_Data->perihal_aktiviti) && $GET_Data->perihal_aktiviti!==''){
    $perihal_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->perihal_aktiviti);
    
    if($condition!=''){
        $condition.=" AND perihal_aktiviti LIKE '%$perihal_aktiviti%'";
    }else{
        $condition.="WHERE perihal_aktiviti LIKE '%$perihal_aktiviti%'";
    }    
}

if(!is_null($GET_Data->kod_program) && $GET_Data->kod_program!==''){
    $kod_program= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->kod_program);
    
    if($condition!=''){
        $condition.=" AND kod_program LIKE '%$kod_program%'";
    }else{
        $condition.="WHERE kod_program LIKE '%$kod_program%'";
    }
    
}

if(!is_null($GET_Data->aktif) && $GET_Data->aktif!==''){
    $aktif= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->aktif);
    
    if($condition!=''){
        $condition.=" AND aktif LIKE '%$aktif%'";
    }else{
        $condition.="WHERE aktif LIKE '%$aktif%'";
    }    
}

$sql=<<<SQL
SELECT
  `kod_aktiviti`,
  `perihal_aktiviti`,
  aktif,
  kod_program,
  parent as induk,
  singkatan,
  status_bahagian
FROM
`tbl_aktiviti_ref`
$condition
SQL;
//echo $sql;exit;
//sleep(3);
$KodAktivitiPagingObj = new KodAktivitiPaging($DBQueryObj);
$KodAktivitiPagingObj->setSQLStatement($sql);
$KodAktivitiPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$KodAktivitiPagingObj->setPageProperty($KodAktivitiPagingObj->getPagingInfo());
$KodAktivitiPagingObj->startPaging($setCurrentPage);
