<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

$kod_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->kod_aktiviti);
$perihal_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->perihal_aktiviti);
$induk= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->induk);
$singkatan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->singkatan);
$program= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->program);
//$aktif= $MagicInputObj->aktif;

$sqlQuery = <<<SQL
SELECT
  `kod_aktiviti`,
  `perihal_aktiviti`
FROM
`tbl_aktiviti_ref`
WHERE `kod_aktiviti`='{$kod_aktiviti}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $err='HTTP/1.1 406 Kod Aktiviti telah wujud!';
    header($err);    
    exit();
}

$sqlQuery2 = <<<SQL
SELECT
* 
FROM
`tbl_aktiviti_ref`
WHERE `parent`='{$kod_aktiviti}'
SQL;

$DBQueryObj->setSQL_Statement($sqlQuery2);
$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    $has_child = 1;
}else{
  $has_child=0;
}

$sql = <<<SQL
INSERT INTO `tbl_aktiviti_ref` (
  `kod_aktiviti`,
  `perihal_aktiviti`,
  `parent`,
  `has_child`,
  `singkatan`,
  `kod_program`     
)
VALUES
  (
    '{$kod_aktiviti}',
    '{$perihal_aktiviti}',
    '{$induk}',
    '{$has_child}',
    '{$singkatan}',
    '{$program}'
  );
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    //echo "Successfully! $rowCount records updated!";
    echo "Successfully added!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
