<?php
header("Access-Control-Allow-Origin: *");
include_once '../../vendor/autoload.php';
include_once '../config/db_connection.php';

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

$condition='';

/**TODO: Filter **/
$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->parent)  && $GET_Data->parent!==''){
    $kod_aktiviti= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->parent);
    $condition.="WHERE kod_aktiviti = '$kod_aktiviti'";    
}

$sql=<<<SQL
SELECT
  `kod_aktiviti`,
  `perihal_aktiviti`,
  `status_bahagian`     
FROM `tbl_aktiviti_ref`
{$condition}
SQL;

$DBQueryObj->setSQL_Statement($sql);

$DBQueryObj->runSQL_Query();

if($DBQueryObj->isHavingRecordRow()){
    echo $DBQueryObj->getRowsInJSON();
}else{
    echo '';
}
