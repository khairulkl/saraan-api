<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';
include_once './Tetapan_Permohonan_Paging.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$DBQueryObj=new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,6,7,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

const PAGING_SIZE=10;
$setCurrentPage=1;
$condition='';

$GET_Data=new MagicInput();
$GET_Data->copy_GET_properties();

if(!is_null($GET_Data->currentPage)){
    $setCurrentPage=$GET_Data->currentPage;
}

/**TODO: Future to filter tetapan by date/year 
if(!is_null($GET_Data->nama)  && $GET_Data->nama!==''){
    $nama= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->nama);
    $condition.="AND Nama LIKE '%$nama%'";
    
}

if(!is_null($GET_Data->nokp) && $GET_Data->nokp!==''){
    $nokp= mysqli_real_escape_string($DBQueryObj->getLink(), $GET_Data->nokp);
    
     if($condition!=''){
        $condition.=" AND NoKP = '$nokp'";
    }else{
        $condition.="AND NoKP = '$nokp'";
    }
}  
 * 
 */

$sql=<<<SQL
SELECT
  `index_sesi_pb`,
  `keterangan`,
  `tarikh_buka_mohon`,
  `tarikh_tutup_mohon`,
  `status_sesi`,
  `status_mohon`
FROM
  `tbl_tetapan_pb`
SQL;
//sleep(3);
$PenggunaPagingObj = new TetapanPermohonanPaging($DBQueryObj);
$PenggunaPagingObj->setSQLStatement($sql);
$PenggunaPagingObj->setPagingProperty(IPagingType::MANUAL, PAGING_SIZE);
$PenggunaPagingObj->setPageProperty($PenggunaPagingObj->getPagingInfo());
$PenggunaPagingObj->startPaging($setCurrentPage);
