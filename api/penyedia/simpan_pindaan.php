<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,Authorization');

/*TODO: (1) include all security headers above*/

include_once '../../vendor/autoload.php';

/*TODO: (2) Include EV Session Container Class*/
include_once '../login/EVSessionHandler.php';

include_once '../config/db_connection.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}


$status_permohonan=3;
$today = date("Y-m-d");

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/*TODO: (3) Read authentication token from front-end request*/
$headers = apache_request_headers();

/*TODO: (4) If app server capture any request header, proceed with authentication*/
if($headers){
    /*TODO: (5) Read header authorization from api request and set as session id*/
    $session_id= mysqli_real_escape_string($DBQueryObj->getLink(), $headers['Authorization']);
    
    $sessionHandler=new EVSessionHandler($DBQueryObj);
    session_set_save_handler($sessionHandler, true);
    session_id($session_id);
    session_start();
    
    if(!isset($_SESSION['icno'])){
        /*TODO: (6) Authentication failed, user is not in session*/
        header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
        echo 'Sesi tidak sah!';
        exit();
    }else{
        if(!in_array($_SESSION['roleID'], [2,3,4,5,11])){
            /*TODO: (6) Authorization failed, user is in session but lack of required access role*/
            header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
            echo 'Peranan tidak sah';
            exit();
        }
    }
}else{
    /*TODO: No header sent by requester or app server failed reading request header*/
    header("{$_SERVER['SERVER_PROTOCOL']} 401 Unauthorized");
    echo 'Sesi tidak sah!';
    exit();
}

/*TODO: (7) Authentication & Authorization is successfull, proceed with api logic*/

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();


$justifikasi_pindaan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->justifikasi_penyedia);
$id_pegawai_proses= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->icno);
$index_permohonan= mysqli_real_escape_string($DBQueryObj->getLink(), $MagicInputObj->index_permohonan);

$sql = <<<SQL
UPDATE 
`tbl_permohonan` 
SET
  `tarikh_proses` = '{$today}',
  `id_pegawai_proses` = '{$id_pegawai_proses}',
  `status_permohonan` = $status_permohonan,
  `justifikasi_pindaan` = '{$justifikasi_pindaan}' 
WHERE `index_permohonan` = '{$index_permohonan}'
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    $getinsertID = $DBCmd->getinsertID();
    //echo "Successfully! $rowCount records updated!";
    $obj = new MagicObject();
    $obj->status = $DBCmd->getAffectedRowCount();
    $obj->getinsertID = $DBCmd->getinsertID();
    $obj->errorMessage = 'Berjaya';
    echo $obj->getJsonString();
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
