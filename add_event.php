<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

include_once './vendor/autoload.php';
include_once './api/config/db_connection_tot.php';

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    echo 'preflight';exit;
}

$MagicInputObj = new MagicInput();
$MagicInputObj->copy_RAW_JSON_properties();
$DBQueryObj = new DBQuery($host, $username, $password, $database_name);


$title=$MagicInputObj->title;
$date= mysqli_real_escape_string($DBQueryObj->getLink(),$MagicInputObj->date);
$time=$MagicInputObj->time;
$location=$MagicInputObj->location;
$description=$MagicInputObj->description;
$category=$MagicInputObj->category;

$sql = <<<SQL
INSERT INTO `vuetot`.`events` (
  `title`,
  `date`,
  `time`,
  `location`,
  `description`,
  `category`
)
VALUES
  (
    '$title',
    '$date',
    '$time',
    '$location',
    '$description',
    '$category'
  );
SQL;

//echo $sql;exit;

$DBCmd = new DBCommand($DBQueryObj);
$DBCmd->executeCustomQueryCommand($sql);

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* Retrieving affected row count on update */
    $rowCount = $DBCmd->getAffectedRowCount();
    echo "Successfully! $rowCount records updated!";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}
